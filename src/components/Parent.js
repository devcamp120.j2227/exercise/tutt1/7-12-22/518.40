import { Component } from "react";
import Display from "./Display";

class Parent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            display: false,
            displayButton: ""
        }
    }

    updateDisplay = () => {
        this.setState({
            display: true,
            displayButton: "none"
        })
    }


    render() {
        return (
            <div className="row text-center">
                <div>{ this.state.display ? <Display style={{display: this.state.displayButton}}/> : null }</div>
                <div className="col"><button className="btn btn-primary" onClick={() => this.updateDisplay()} style={{display: this.state.displayButton}}>Create Component</button></div>
            </div>
        )
    }
}

export default Parent