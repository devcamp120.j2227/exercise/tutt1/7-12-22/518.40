import { Component } from "react";

class Display extends Component {
    constructor(props) {
        super(props);

        this.state = {
            display: false,
            displayButton: ""
        }
    }

    updateDisplay = () => {
        this.setState({
            display: true,
            displayButton: "none"
        })
    }

    componentWillMount() {
        console.log("Component Will Mount");
    }

    componentDidMount() {
        console.log('Component Did Mount');
    }

    componentWillUnmount() {
        console.log("Component will Unmount");
    }

    render() {
        return (
            <div style={{display: this.state.displayButton}}>
                <h1>I exist !</h1>
                <div className="col"><button className="btn btn-danger" onClick={this.updateDisplay}>Destroyed Component</button></div>
            </div>
        )
    }
}

export default Display